/*
 *
 * ----- Program: Dictionary - P1 - EDA II                                              -----
 * ----- Authors: Sergio Besses (NIA 195660) i Marc Amorós (NIA 195317)                 -----
 * ----- Data: 14-15/06/2017                                                            -----
 * ----- Functionality: Archivo principal de la ejecución del programa                  -----
 *
 *
 *
 * */
#include <stdio.h>
#include <stdlib.h>
#include "header.h"
#include "BinaryTree.h"

int main() {


    /*
     * Estos dos vectores están creados especialmente para que funcione nuestro algoritmo de creación
     * del árbol de decisión.
     *
     * El arbol se crea recursivamente de izquierda a derecha. Primero crea el nodo bajo-izquierdo del árbol, luego
     * el de su derecha y luego el de arriba, y así recursivamente, haciendo que el orden de creación quede así:
     *
     *                ( 14 )
     *            /            \
     *         (6)            (13)
     *       /     \       /        \
     *     (2)    (5)     (9)      (12)
     *     /  \   /  \    /  \    /    \
     *   (0) (1)(3)(4)  (7) (8) (10)  (11)    (Ejemplo árbol 1)
     *
     *
     * El primer elemento del array es el primer nodo que va a ser creado del arbol.
     *
     *
     *                 ( 1 )
     *            /              \
     *          (2)             (4)
     *        /     \        /       \
     *      (3)   (10)     (5)       (6)
     *     /  \   /  \    /   \     /  \     (N === null)
     *   (8) (9)(N) (N) (11)(12) (13)  (14)   (Árbol original)
     *
     * Cada elemento es el índice de un vector de strings. Los nodos que deberían ser nulos tienen
     * un índice de 6, ya que en la posición 6 del vector de strings se encuentra un string vacío ("").
     *
     * Nótese que cada elemento tiene una cifra inferior, para indexar también el elemento 0 del array de strings.
     *
    */


    // Árbol 1
    int m1[SIZE_ARRAY_1] = {7,8,2,6,6,9,1,10,11,4,12,13,5,3,0};
    // Árbol 2
    int m2[SIZE_ARRAY_2] =  {6,6,6,6,6,6,8,6,6,9,6,6,14,7,3,6,6,
                             12,6,6,13,2,6,6,10,13,11,3,4,1,0};


    //Aquí se crean las variables opt, que se usará para manejar el menu, y los árboles 1 y 2,
    //en los cuales ya se atribuye memoria.

    int opt;
    BinaryTree *tree1 = malloc(sizeof(BinaryTree));
    BinaryTree *tree2 = malloc(sizeof(BinaryTree));


    //Se llama a la función recursiva 'add' para que llene los árboles binarios.
    tree1 = add(tree1, ORIGINAL_DEPTH, ARRAY_PARAM_1, LEAF_LEVEL_1, m1, TRUE);
    tree2 = add(tree2, ORIGINAL_DEPTH, ARRAY_PARAM_2, LEAF_LEVEL_2, m2, FALSE);


    //Éste es el menú de opciones

    do {
        printf("\n\nElige una opción del menú: (0 para salir) \n");
        printf("1. Imprimir árbol 1\n");
        printf("2. Imprimir árbol 2\n");
        printf("3. Interactuar con un árbol 1\n");
        printf("4. Interactuar con un árbol 2\n");
        printf("Option: ");
        scanf("%d", &opt);

        switch (opt){
            case PRINT_TREE_1:
                print_tree(tree1, ORIGINAL_DEPTH);
                break;
            case PRINT_TREE_2:
                print_tree(tree2, ORIGINAL_DEPTH);
                break;
            case PLAY_TREE_1:
                tree_decision(tree1);
                break;
            case PLAY_TREE_2:
                tree_decision(tree2);
                break;
            case EXIT_OPTION:
                break;
            default:
                printf("Opción no válida! \n");
                break;
        }

    } while(opt != EXIT_OPTION);


    //Finalmente se libera de la memória el espacio reservado para los árboles 1 y 2.
    free_tree(tree1);
    free_tree(tree2);

    return 0;
}