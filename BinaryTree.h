/*
 *
 * ----- Program: Dictionary - P1 - EDA II                                              -----
 * ----- Authors: Sergio Besses (NIA 195660) i Marc Amorós (NIA 195317)                 -----
 * ----- Data: 14-15/06/2017                                                            -----
 * ----- Functionality: Header de la librería de gestión del árbol binario              -----
 *
 *
 *
 * */
#ifndef BINARYTREE_H
#define BINARYTREE_H

#include "header.h"

/*
 * Tarea 1
 * Especifique el tipo árbol de decisión binario
 */


/*
 * -- Estructura de un nodo del árbol --
 * Campos:
 * - data           Este campo lo usaremos para guardar un string / char
 * - left, right    Estos dos campos son lo que unen el nodo con sus "hijos"
 *
 *
 * */

typedef struct BinaryTree {

    char *data;
    struct BinaryTree *left, *right;

} BinaryTree;


/*
 * Tarea 2
 * Implemente el árbol de decisión binario en C.
 */

BinaryTree *create_empty_tree(int n, int isTheFirstTree);
BinaryTree* create_tree(BinaryTree* left, char* content, BinaryTree* right);
BinaryTree* left_tree(BinaryTree* tree);
BinaryTree* right_tree(BinaryTree* tree);
int is_empty(BinaryTree* tree);
int is_leaf(BinaryTree *tree);


/*
 * Tarea 3
 * Implemente una función que dado un árbol de decisión binario imprima
 *  de forma apropiada el árbol.
 */

void print_tree(BinaryTree* tree, int depth);



/*
 * Tarea 4
 * Implemente un programa para leer, a partir de un fichero de texto,
 * la estructura de un árbol de decisión binario y retorne
 * el árbol instanciado apropiadamente.
 */

//Option A
BinaryTree* add(BinaryTree *tree, int depth, int x, int leafLevel, int *m, int isTheFirstTree);

/*
 * Tarea 5
 * Implemente un programa que, dado un árbol de decisión instanciado, interactúe por terminal
 * con un usuario.
 *
 * */
void tree_decision(BinaryTree *tree);

/*
 *
 *
 *
*/

void free_tree(BinaryTree *tree);

#endif