/*
 *
 * ----- Program: Dictionary - P1 - EDA II                                              -----
 * ----- Authors: Sergio Besses (NIA 195660) i Marc Amorós (NIA 195317)                 -----
 * ----- Data: 14-15/06/2017                                                            -----
 * ----- Functionality: Declaración de constantes globales                              -----
 *
 *
 *
 * */
#ifndef PRACTICA2_HEADER_H
#define PRACTICA2_HEADER_H

/* Macros globales */
#define FALSE 0
#define TRUE 1

#define ORIGINAL_DEPTH 0
#define STRING_VECTOR_SIZE_1 14
#define STRING_VECTOR_SIZE_2 15
#define ANSWER_SIZE 4
#define NEGATION_ANSWER_SIZE 2
#define AFIRMATIVE_ANSWER_SIZE 3

/* Árbol 1*/
#define SIZE_ARRAY_1 15
#define ARRAY_PARAM_1 14
#define LEAF_LEVEL_1 3


/* Árbol 2*/
#define SIZE_ARRAY_2 31
#define ARRAY_PARAM_2 30
#define LEAF_LEVEL_2 4

/* Macros del menú*/
#define PRINT_TREE_1 1
#define PRINT_TREE_2 2
#define PLAY_TREE_1 3
#define PLAY_TREE_2 4
#define EXIT_OPTION 0



#endif
