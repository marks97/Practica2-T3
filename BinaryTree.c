/*
 *
 * ----- Program: Dictionary - P1 - EDA II                                              -----
 * ----- Authors: Sergio Besses (NIA 195660) i Marc Amorós (NIA 195317)                 -----
 * ----- Data: 14-15/06/2017                                                            -----
 * ----- Functionality: Librería de gestión del árbol binario                           -----
 *
 *
 *
 * */
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include "BinaryTree.h"


/*
 * Esta función recibe como parámetro un numero entero 'n', y un valor
 * booleano (isTheFirstTree).
 *
 * La función sirve para asignar devolver un string determinado, en función
 * de si el árbol es el 1ro o el 2do, y en función del entero 'n'.
 *
 *
 *
 * */


char *assignDataToTree(int n, int isTheFirstTree) {


    char *m1[STRING_VECTOR_SIZE_1] =
                    {"Are you vegetarian?",
                    "Do you like tomatoes?",
                    "Do you like olive oil?",
                    "Do you like beef?",
                    "Do you like potatoes?",
                    "Do you like spicy food?",
                    "",
                    "I suggest bread & tomatoes",
                    "I suggest tomato soup",
                    "I suggest green salad",
                    "I suggest grilled beef & french fries",
                    "I suggest grilled beef & tomato salad",
                    "I suggest chicken tikka",
                    "I suggest fried chicken"
    };

    char *m2[STRING_VECTOR_SIZE_2]
                    = {"Is your age <= 16?",
                    "Is your age > 16 and <= 50 ?",
                    "Do you like suspense?",
                    "Do you like musicals?",
                    "Do you like romance?",
                    "Do you like thrillers?", "",
                    "Do you like superheroes?",
                    "Watch Frozen",
                    "Watch Spiderman",
                    "Watch Shakespeare in Love",
                    "Watch Fast and Furious 100",
                    "Watch the Davinci Code",
                    "Watch La La Land",
                    "Watch Back to Future",
    };

    if(isTheFirstTree)
        return m1[n];
    else
        return m2[n];

}

/*
 * -- Creación de un árbol vacío --
 * 1. Creamos una nueva instancia de nodo que será nuestra raíz.
 * 2. La definimos como nula y la devolvemos.
 *
 *
 * */
BinaryTree *create_empty_tree(int n, int isTheFirstTree) {

    BinaryTree *node = malloc(sizeof(BinaryTree));
    node->right = NULL;
    node->left = NULL;
    node->data = assignDataToTree(n, isTheFirstTree);
    return node;
}

/*
 * -- Crear un árbol binario --
 * 1. Creamos un nodo de tipo árbol binario mediante la asignación de memoria que le corresponde.
 * 2. Asignamos los parámetros a los campos del nodo y lo devolvemos.
 *
 *
 * */
BinaryTree *create_tree(BinaryTree *left, char *content, BinaryTree *right) {

    BinaryTree *node = malloc(sizeof(BinaryTree));

    node->data = content;
    node->left = left;
    node->right = right;

    return node;
}


/*
 * -- Devolvemos el hijo izquierdo del nodo --
 *
 *
 * */
BinaryTree *left_tree(BinaryTree *tree) {
    return tree->left;
}

/*
 * -- Devolvemos el hijo derecho del nodo --
 *
 *
 * */
BinaryTree *right_tree(BinaryTree *tree) {
    return tree->right;
}

/*
 * -- Comprobar si un árbol está vacío --
 *
 *
 * */
int is_empty(BinaryTree *tree) {
    return tree == NULL;
}

/*
 * Esta función es la encargada de, de manera recursiva, crear y llenar un árbol de decisión binario.
 *
 * Recibe como parámetros:
 *
 * 1. Un árbol, en 1er caso el árbol creado en el Main.
 * 2. Un entero depth, que sirve para saber la profundidad de la recursion
 * 3. Un entero 'x', el cual sirve para calcular que elemento vamos a guardar dentro de un determinado nodo
 * 4. Un entero 'leafLevel', el cual sirve para saber en que nivel de profundidad están las hojas.
 * 5. Una vector m, donde estan los índices de los strings que guardaremos en los nodos.
 * 6. Un boolean 'isTheFirstTree', para saber si estamos operando con el primer, o el segundo árbol.
 *
 * Funcionamiento:
 *
 * Como ya hemos explicado en el Main, para llenar un árbol, lo haremos de izquierda a derecha, y de abajo a arriba.
 *
 * 1. Primero comprobamos la profundidad de la recursión con la variable 'depth'. Si ésta es igual
 *    al numero del nivel de las hojas, significa que hemos llegado a uno de los casos base.
 *
 * 2. Si el depth no es igual al caso base, tenemos que seguir bajando nodos (por la parte de la izquierda),
 *    para así llegar al nodo base inferior izquierdo.
 *
 * 3. Una vez llenado el nodo izquierdo (caso base) llenamos el de su derecha. De ésta manera,
 *    con el backtracking recursivo se va llenando el árbol de manera automática.
 *
 * 4. Cada vez que tengamos un nodo izquierdo y derecho crados, los juntaremos con la función create_tree,
 *    y devolveremos dicho nodo a la función anterior.
 *
 * 5. Para saber que string tiene que ser llenado en cada momento disponemos del índice 'x'. Con
 *    un par de operaciones podemos saber en cada momento que posición del array (el cual también es
 *    pasado como parámetro) es la actual.
 *
 *    n = 2^(4 - 0 + 1)    Ejemplo primer arbol, profundidad 0.
 *
 *    Cuando aumentamos de profundidad:

 *    x = x - n
 *
 *    Cuando queremos obtener el índice del nodo de la derecha
 *
 *    x = x + n - 1
 *
 *
*/

BinaryTree* add(BinaryTree *tree, int depth, int x, int leafLevel, int *m, int isTheFirstTree) {

    if(depth == leafLevel) {

        return create_empty_tree(m[x], isTheFirstTree);

    } else {

        char *c = assignDataToTree(m[x], isTheFirstTree);

        depth++;
        int n = (int)pow(2, (leafLevel-depth + 1));
        x -= n;

        BinaryTree *leftTree = malloc(sizeof(BinaryTree));
        leftTree = add(leftTree, depth,  x, leafLevel, m, isTheFirstTree);

        x += n-1;

        BinaryTree *rightTree = malloc(sizeof(BinaryTree));
        rightTree = add(rightTree, depth,  x, leafLevel, m, isTheFirstTree);

        tree = create_tree(leftTree, c, rightTree);

    }

    return tree;
}

/*
 * -- Comprobar si un nodo es una hoja --
 * 1. Comprobamos si el hijo es nulo ya que en cualquier hoja los dos lo serán.
 * Si no lo tiene, es una hoja por lo que devolvemos true.
 * 2. Comprobamos si el hijo tiene el campo data vacío. Esto es para las hojas
 * que no están en el último nivel de profunidad.
 * Si el campo está vacío, es una hoja por lo que devolvemos true.
 * 3. Sino, devolvemos false.
 *
 *
 * */
int is_leaf(BinaryTree *tree) {
    if(tree->left == NULL){
        return TRUE;
    }
    if(tree->left->data[0] == '\0'){
        return TRUE;
    }
    return FALSE;
}

/*
* -- Imprimir espacios según profunidad --
*
*/
void print_spaces(int spaces){
    int i;
    for (i = 0; i < spaces; i++) {
        printf("   ");
    }

}

/*
 * -- Imprimir un árbol --
 *
 * Para poder imprimir un árbol usamos un algoritmo recursivo de tipo Pre-orden
 * 1. Definimos el caso base (si se trata de una hoja). En ese caso, imprimimos
 * los espacios correspondientes a la profundidad y un mensaje diferente.
 * 2. Si es un nodo, imprimimos los espacios de la profunidad e imprimimos un mensaje
 * con un "sí" o "no" dependiendo si luego entramos en el hijo izquierdo o derecho.
 * 3. En cada iteración, sumamos un nivel de profundidad antes de entrar al hijo y restamos
 * un nivel en el caso de volver al nodo padre.
 *
 *
 * */
void print_tree(BinaryTree *tree, int depth) {
    if (is_leaf(tree)) { // Caso base si es una hoja
        print_spaces(depth);
        printf("Le recomendamos: %s \n", tree->data);
    }
    else{
        print_spaces(depth);
        printf("Respuesta a la pregunta %s = sí\n", tree->data);
        depth++;
        print_tree(tree->left, depth);

        depth--;

        print_spaces(depth);
        printf("Respuesta a la pregunta %s = no\n", tree->data);
        depth++;
        print_tree(tree->right, depth);

    }
}

/*
 * -- Usar de toma de decisiones --
 * 1. Imprimimos un mensaje con las instrucciones.
 * 2. Mediante un bucle, recorremos el árbol siempre que no esté vacío.
 * 3. En cada iteración, comprobamos que el nodo sea una hoja o no. En caso de
 * ser una hoja, mostramos una respuesta final y finalizamos.
 * 4. En caso de no ser una hoja, guardamos el input del usuario y comprobamos
 * si la respuesta es "no". En caso de ser "no", vamos hacia a la derecha y viceversa.
 *
 *
 * */
void tree_decision(BinaryTree *tree) {

    printf("\n\nUsa \'yes\' o \'no\' para responder a las preguntas\n");

    while (!is_empty(tree)){

        if (is_leaf(tree)) {
            printf("System: then %s\n", tree->data);
            printf(" --- End of Tree --");
            break;
        }

        char answer[ANSWER_SIZE];
        printf("System: %s\n", tree->data);
        printf("User: ");
        scanf("%s", answer);
        getchar();

        if (strncmp(answer, "no", NEGATION_ANSWER_SIZE) == 0) {
            tree = right_tree(tree);
        } else if (strncmp(answer, "yes", AFIRMATIVE_ANSWER_SIZE) == 0){
            tree = left_tree(tree);
        } else{
            printf("wrong answer.\n");
        }
    }
}

/*
 * Esta función sirve para, de manera recursiva, liberar la memória en todos los nodos
 * de un árbol binario.
 *
 * El caso base se da cuando el nodo es un nodo correspondiente a una hoja.
 * Cuando se da el caso se libera el nodo de la memoria, y se le asigna un NULL.
 *
 * */

void free_tree(BinaryTree *tree){

    if(is_leaf(tree)) {
        tree = NULL;
        free(tree);
        return;
    }

    free_tree(tree->left);
    free_tree(tree->right);

    tree = NULL;
    free(tree);

    return;

}

