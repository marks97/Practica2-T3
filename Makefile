# Este es el MakeFile de la práctica 1 sobre el Árbol Binario de decisiones
# del grupo 64 formado por Marc Amorós y Sergio Besses
# EDA II - 06/2017

# Variables
files = main.o BinaryTree.o

# Regla para compilar el programa principal
tree: $(files)
	cc -o tree $(files)

# Dependencias de los archivos
main.o: header.h BinaryTree.h

BinaryTree.o: header.h

# Eliminamos todo y mostramos un mensaje
clean:
	rm -f tree *.o
	@echo "Archivos: $(files) eliminados."